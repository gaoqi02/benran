package com.newvery.model;

import java.io.Serializable;
import java.util.Date;

public class TbUser implements Serializable {
    private Integer id;

    private String username;

    private String realname;

    private String email;

    private String mobile;

    private String telephone;

    private String address;

    private String password;

    private Integer locked;

    private Date registerdate;

    private Integer deleted;

    private Date lastvisitdate;

    private String validatecode;

    private Date outdate;

    private Integer articlenum;

    private Date modifytime;

    private Integer vipdates;

    private Integer usergroupid;

    private Integer visitfloder;

    private Integer visitchart;

    private Integer groupadmin;

    private Integer tagFlag;

    private String icon;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Integer getLocked() {
        return locked;
    }

    public void setLocked(Integer locked) {
        this.locked = locked;
    }

    public Date getRegisterdate() {
        return registerdate;
    }

    public void setRegisterdate(Date registerdate) {
        this.registerdate = registerdate;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Date getLastvisitdate() {
        return lastvisitdate;
    }

    public void setLastvisitdate(Date lastvisitdate) {
        this.lastvisitdate = lastvisitdate;
    }

    public String getValidatecode() {
        return validatecode;
    }

    public void setValidatecode(String validatecode) {
        this.validatecode = validatecode == null ? null : validatecode.trim();
    }

    public Date getOutdate() {
        return outdate;
    }

    public void setOutdate(Date outdate) {
        this.outdate = outdate;
    }

    public Integer getArticlenum() {
        return articlenum;
    }

    public void setArticlenum(Integer articlenum) {
        this.articlenum = articlenum;
    }

    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }

    public Integer getVipdates() {
        return vipdates;
    }

    public void setVipdates(Integer vipdates) {
        this.vipdates = vipdates;
    }

    public Integer getUsergroupid() {
        return usergroupid;
    }

    public void setUsergroupid(Integer usergroupid) {
        this.usergroupid = usergroupid;
    }

    public Integer getVisitfloder() {
        return visitfloder;
    }

    public void setVisitfloder(Integer visitfloder) {
        this.visitfloder = visitfloder;
    }

    public Integer getVisitchart() {
        return visitchart;
    }

    public void setVisitchart(Integer visitchart) {
        this.visitchart = visitchart;
    }

    public Integer getGroupadmin() {
        return groupadmin;
    }

    public void setGroupadmin(Integer groupadmin) {
        this.groupadmin = groupadmin;
    }

    public Integer getTagFlag() {
        return tagFlag;
    }

    public void setTagFlag(Integer tagFlag) {
        this.tagFlag = tagFlag;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", realname=").append(realname);
        sb.append(", email=").append(email);
        sb.append(", mobile=").append(mobile);
        sb.append(", telephone=").append(telephone);
        sb.append(", address=").append(address);
        sb.append(", password=").append(password);
        sb.append(", locked=").append(locked);
        sb.append(", registerdate=").append(registerdate);
        sb.append(", deleted=").append(deleted);
        sb.append(", lastvisitdate=").append(lastvisitdate);
        sb.append(", validatecode=").append(validatecode);
        sb.append(", outdate=").append(outdate);
        sb.append(", articlenum=").append(articlenum);
        sb.append(", modifytime=").append(modifytime);
        sb.append(", vipdates=").append(vipdates);
        sb.append(", usergroupid=").append(usergroupid);
        sb.append(", visitfloder=").append(visitfloder);
        sb.append(", visitchart=").append(visitchart);
        sb.append(", groupadmin=").append(groupadmin);
        sb.append(", tagFlag=").append(tagFlag);
        sb.append(", icon=").append(icon);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}