package com.newvery.mapper;

import com.newvery.model.TbUser;
import com.newvery.domain.TbUserCriteria;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

public interface TbUserMapper {
    int countByExample(TbUserCriteria example);

    int deleteByExample(TbUserCriteria example);

    int deleteByPrimaryKey(Integer id);

    int insert(TbUser record);

    int insertSelective(TbUser record);

    List<TbUser> selectByExampleWithBLOBsWithRowbounds(TbUserCriteria example, RowBounds rowBounds);

    List<TbUser> selectByExampleWithBLOBs(TbUserCriteria example);

    List<TbUser> selectByExampleWithRowbounds(TbUserCriteria example, RowBounds rowBounds);

    List<TbUser> selectByExample(TbUserCriteria example);

    TbUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TbUser record, @Param("example") TbUserCriteria example);

    int updateByExampleWithBLOBs(@Param("record") TbUser record, @Param("example") TbUserCriteria example);

    int updateByExample(@Param("record") TbUser record, @Param("example") TbUserCriteria example);

    int updateByPrimaryKeySelective(TbUser record);

    int updateByPrimaryKeyWithBLOBs(TbUser record);

    int updateByPrimaryKey(TbUser record);
}