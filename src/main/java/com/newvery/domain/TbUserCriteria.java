package com.newvery.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TbUserCriteria {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TbUserCriteria() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("userName =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("userName <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("userName >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("userName <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("userName like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("userName not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("userName in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("userName not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andRealnameIsNull() {
            addCriterion("realName is null");
            return (Criteria) this;
        }

        public Criteria andRealnameIsNotNull() {
            addCriterion("realName is not null");
            return (Criteria) this;
        }

        public Criteria andRealnameEqualTo(String value) {
            addCriterion("realName =", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameNotEqualTo(String value) {
            addCriterion("realName <>", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameGreaterThan(String value) {
            addCriterion("realName >", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameGreaterThanOrEqualTo(String value) {
            addCriterion("realName >=", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameLessThan(String value) {
            addCriterion("realName <", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameLessThanOrEqualTo(String value) {
            addCriterion("realName <=", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameLike(String value) {
            addCriterion("realName like", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameNotLike(String value) {
            addCriterion("realName not like", value, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameIn(List<String> values) {
            addCriterion("realName in", values, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameNotIn(List<String> values) {
            addCriterion("realName not in", values, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameBetween(String value1, String value2) {
            addCriterion("realName between", value1, value2, "realname");
            return (Criteria) this;
        }

        public Criteria andRealnameNotBetween(String value1, String value2) {
            addCriterion("realName not between", value1, value2, "realname");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNull() {
            addCriterion("telephone is null");
            return (Criteria) this;
        }

        public Criteria andTelephoneIsNotNull() {
            addCriterion("telephone is not null");
            return (Criteria) this;
        }

        public Criteria andTelephoneEqualTo(String value) {
            addCriterion("telephone =", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotEqualTo(String value) {
            addCriterion("telephone <>", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThan(String value) {
            addCriterion("telephone >", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("telephone >=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThan(String value) {
            addCriterion("telephone <", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLessThanOrEqualTo(String value) {
            addCriterion("telephone <=", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneLike(String value) {
            addCriterion("telephone like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotLike(String value) {
            addCriterion("telephone not like", value, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneIn(List<String> values) {
            addCriterion("telephone in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotIn(List<String> values) {
            addCriterion("telephone not in", values, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneBetween(String value1, String value2) {
            addCriterion("telephone between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andTelephoneNotBetween(String value1, String value2) {
            addCriterion("telephone not between", value1, value2, "telephone");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andLockedIsNull() {
            addCriterion("locked is null");
            return (Criteria) this;
        }

        public Criteria andLockedIsNotNull() {
            addCriterion("locked is not null");
            return (Criteria) this;
        }

        public Criteria andLockedEqualTo(Integer value) {
            addCriterion("locked =", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotEqualTo(Integer value) {
            addCriterion("locked <>", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedGreaterThan(Integer value) {
            addCriterion("locked >", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedGreaterThanOrEqualTo(Integer value) {
            addCriterion("locked >=", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedLessThan(Integer value) {
            addCriterion("locked <", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedLessThanOrEqualTo(Integer value) {
            addCriterion("locked <=", value, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedIn(List<Integer> values) {
            addCriterion("locked in", values, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotIn(List<Integer> values) {
            addCriterion("locked not in", values, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedBetween(Integer value1, Integer value2) {
            addCriterion("locked between", value1, value2, "locked");
            return (Criteria) this;
        }

        public Criteria andLockedNotBetween(Integer value1, Integer value2) {
            addCriterion("locked not between", value1, value2, "locked");
            return (Criteria) this;
        }

        public Criteria andRegisterdateIsNull() {
            addCriterion("registerDate is null");
            return (Criteria) this;
        }

        public Criteria andRegisterdateIsNotNull() {
            addCriterion("registerDate is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterdateEqualTo(Date value) {
            addCriterion("registerDate =", value, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateNotEqualTo(Date value) {
            addCriterion("registerDate <>", value, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateGreaterThan(Date value) {
            addCriterion("registerDate >", value, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateGreaterThanOrEqualTo(Date value) {
            addCriterion("registerDate >=", value, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateLessThan(Date value) {
            addCriterion("registerDate <", value, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateLessThanOrEqualTo(Date value) {
            addCriterion("registerDate <=", value, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateIn(List<Date> values) {
            addCriterion("registerDate in", values, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateNotIn(List<Date> values) {
            addCriterion("registerDate not in", values, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateBetween(Date value1, Date value2) {
            addCriterion("registerDate between", value1, value2, "registerdate");
            return (Criteria) this;
        }

        public Criteria andRegisterdateNotBetween(Date value1, Date value2) {
            addCriterion("registerDate not between", value1, value2, "registerdate");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateIsNull() {
            addCriterion("lastvisitDate is null");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateIsNotNull() {
            addCriterion("lastvisitDate is not null");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateEqualTo(Date value) {
            addCriterion("lastvisitDate =", value, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateNotEqualTo(Date value) {
            addCriterion("lastvisitDate <>", value, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateGreaterThan(Date value) {
            addCriterion("lastvisitDate >", value, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateGreaterThanOrEqualTo(Date value) {
            addCriterion("lastvisitDate >=", value, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateLessThan(Date value) {
            addCriterion("lastvisitDate <", value, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateLessThanOrEqualTo(Date value) {
            addCriterion("lastvisitDate <=", value, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateIn(List<Date> values) {
            addCriterion("lastvisitDate in", values, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateNotIn(List<Date> values) {
            addCriterion("lastvisitDate not in", values, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateBetween(Date value1, Date value2) {
            addCriterion("lastvisitDate between", value1, value2, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andLastvisitdateNotBetween(Date value1, Date value2) {
            addCriterion("lastvisitDate not between", value1, value2, "lastvisitdate");
            return (Criteria) this;
        }

        public Criteria andValidatecodeIsNull() {
            addCriterion("validateCode is null");
            return (Criteria) this;
        }

        public Criteria andValidatecodeIsNotNull() {
            addCriterion("validateCode is not null");
            return (Criteria) this;
        }

        public Criteria andValidatecodeEqualTo(String value) {
            addCriterion("validateCode =", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeNotEqualTo(String value) {
            addCriterion("validateCode <>", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeGreaterThan(String value) {
            addCriterion("validateCode >", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeGreaterThanOrEqualTo(String value) {
            addCriterion("validateCode >=", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeLessThan(String value) {
            addCriterion("validateCode <", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeLessThanOrEqualTo(String value) {
            addCriterion("validateCode <=", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeLike(String value) {
            addCriterion("validateCode like", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeNotLike(String value) {
            addCriterion("validateCode not like", value, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeIn(List<String> values) {
            addCriterion("validateCode in", values, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeNotIn(List<String> values) {
            addCriterion("validateCode not in", values, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeBetween(String value1, String value2) {
            addCriterion("validateCode between", value1, value2, "validatecode");
            return (Criteria) this;
        }

        public Criteria andValidatecodeNotBetween(String value1, String value2) {
            addCriterion("validateCode not between", value1, value2, "validatecode");
            return (Criteria) this;
        }

        public Criteria andOutdateIsNull() {
            addCriterion("outDate is null");
            return (Criteria) this;
        }

        public Criteria andOutdateIsNotNull() {
            addCriterion("outDate is not null");
            return (Criteria) this;
        }

        public Criteria andOutdateEqualTo(Date value) {
            addCriterion("outDate =", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateNotEqualTo(Date value) {
            addCriterion("outDate <>", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateGreaterThan(Date value) {
            addCriterion("outDate >", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateGreaterThanOrEqualTo(Date value) {
            addCriterion("outDate >=", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateLessThan(Date value) {
            addCriterion("outDate <", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateLessThanOrEqualTo(Date value) {
            addCriterion("outDate <=", value, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateIn(List<Date> values) {
            addCriterion("outDate in", values, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateNotIn(List<Date> values) {
            addCriterion("outDate not in", values, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateBetween(Date value1, Date value2) {
            addCriterion("outDate between", value1, value2, "outdate");
            return (Criteria) this;
        }

        public Criteria andOutdateNotBetween(Date value1, Date value2) {
            addCriterion("outDate not between", value1, value2, "outdate");
            return (Criteria) this;
        }

        public Criteria andArticlenumIsNull() {
            addCriterion("articleNum is null");
            return (Criteria) this;
        }

        public Criteria andArticlenumIsNotNull() {
            addCriterion("articleNum is not null");
            return (Criteria) this;
        }

        public Criteria andArticlenumEqualTo(Integer value) {
            addCriterion("articleNum =", value, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumNotEqualTo(Integer value) {
            addCriterion("articleNum <>", value, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumGreaterThan(Integer value) {
            addCriterion("articleNum >", value, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumGreaterThanOrEqualTo(Integer value) {
            addCriterion("articleNum >=", value, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumLessThan(Integer value) {
            addCriterion("articleNum <", value, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumLessThanOrEqualTo(Integer value) {
            addCriterion("articleNum <=", value, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumIn(List<Integer> values) {
            addCriterion("articleNum in", values, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumNotIn(List<Integer> values) {
            addCriterion("articleNum not in", values, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumBetween(Integer value1, Integer value2) {
            addCriterion("articleNum between", value1, value2, "articlenum");
            return (Criteria) this;
        }

        public Criteria andArticlenumNotBetween(Integer value1, Integer value2) {
            addCriterion("articleNum not between", value1, value2, "articlenum");
            return (Criteria) this;
        }

        public Criteria andModifytimeIsNull() {
            addCriterion("modifyTime is null");
            return (Criteria) this;
        }

        public Criteria andModifytimeIsNotNull() {
            addCriterion("modifyTime is not null");
            return (Criteria) this;
        }

        public Criteria andModifytimeEqualTo(Date value) {
            addCriterion("modifyTime =", value, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeNotEqualTo(Date value) {
            addCriterion("modifyTime <>", value, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeGreaterThan(Date value) {
            addCriterion("modifyTime >", value, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modifyTime >=", value, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeLessThan(Date value) {
            addCriterion("modifyTime <", value, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeLessThanOrEqualTo(Date value) {
            addCriterion("modifyTime <=", value, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeIn(List<Date> values) {
            addCriterion("modifyTime in", values, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeNotIn(List<Date> values) {
            addCriterion("modifyTime not in", values, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeBetween(Date value1, Date value2) {
            addCriterion("modifyTime between", value1, value2, "modifytime");
            return (Criteria) this;
        }

        public Criteria andModifytimeNotBetween(Date value1, Date value2) {
            addCriterion("modifyTime not between", value1, value2, "modifytime");
            return (Criteria) this;
        }

        public Criteria andVipdatesIsNull() {
            addCriterion("vipDates is null");
            return (Criteria) this;
        }

        public Criteria andVipdatesIsNotNull() {
            addCriterion("vipDates is not null");
            return (Criteria) this;
        }

        public Criteria andVipdatesEqualTo(Integer value) {
            addCriterion("vipDates =", value, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesNotEqualTo(Integer value) {
            addCriterion("vipDates <>", value, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesGreaterThan(Integer value) {
            addCriterion("vipDates >", value, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesGreaterThanOrEqualTo(Integer value) {
            addCriterion("vipDates >=", value, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesLessThan(Integer value) {
            addCriterion("vipDates <", value, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesLessThanOrEqualTo(Integer value) {
            addCriterion("vipDates <=", value, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesIn(List<Integer> values) {
            addCriterion("vipDates in", values, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesNotIn(List<Integer> values) {
            addCriterion("vipDates not in", values, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesBetween(Integer value1, Integer value2) {
            addCriterion("vipDates between", value1, value2, "vipdates");
            return (Criteria) this;
        }

        public Criteria andVipdatesNotBetween(Integer value1, Integer value2) {
            addCriterion("vipDates not between", value1, value2, "vipdates");
            return (Criteria) this;
        }

        public Criteria andUsergroupidIsNull() {
            addCriterion("userGroupId is null");
            return (Criteria) this;
        }

        public Criteria andUsergroupidIsNotNull() {
            addCriterion("userGroupId is not null");
            return (Criteria) this;
        }

        public Criteria andUsergroupidEqualTo(Integer value) {
            addCriterion("userGroupId =", value, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidNotEqualTo(Integer value) {
            addCriterion("userGroupId <>", value, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidGreaterThan(Integer value) {
            addCriterion("userGroupId >", value, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidGreaterThanOrEqualTo(Integer value) {
            addCriterion("userGroupId >=", value, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidLessThan(Integer value) {
            addCriterion("userGroupId <", value, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidLessThanOrEqualTo(Integer value) {
            addCriterion("userGroupId <=", value, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidIn(List<Integer> values) {
            addCriterion("userGroupId in", values, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidNotIn(List<Integer> values) {
            addCriterion("userGroupId not in", values, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidBetween(Integer value1, Integer value2) {
            addCriterion("userGroupId between", value1, value2, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andUsergroupidNotBetween(Integer value1, Integer value2) {
            addCriterion("userGroupId not between", value1, value2, "usergroupid");
            return (Criteria) this;
        }

        public Criteria andVisitfloderIsNull() {
            addCriterion("visitFloder is null");
            return (Criteria) this;
        }

        public Criteria andVisitfloderIsNotNull() {
            addCriterion("visitFloder is not null");
            return (Criteria) this;
        }

        public Criteria andVisitfloderEqualTo(Integer value) {
            addCriterion("visitFloder =", value, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderNotEqualTo(Integer value) {
            addCriterion("visitFloder <>", value, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderGreaterThan(Integer value) {
            addCriterion("visitFloder >", value, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderGreaterThanOrEqualTo(Integer value) {
            addCriterion("visitFloder >=", value, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderLessThan(Integer value) {
            addCriterion("visitFloder <", value, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderLessThanOrEqualTo(Integer value) {
            addCriterion("visitFloder <=", value, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderIn(List<Integer> values) {
            addCriterion("visitFloder in", values, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderNotIn(List<Integer> values) {
            addCriterion("visitFloder not in", values, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderBetween(Integer value1, Integer value2) {
            addCriterion("visitFloder between", value1, value2, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitfloderNotBetween(Integer value1, Integer value2) {
            addCriterion("visitFloder not between", value1, value2, "visitfloder");
            return (Criteria) this;
        }

        public Criteria andVisitchartIsNull() {
            addCriterion("visitChart is null");
            return (Criteria) this;
        }

        public Criteria andVisitchartIsNotNull() {
            addCriterion("visitChart is not null");
            return (Criteria) this;
        }

        public Criteria andVisitchartEqualTo(Integer value) {
            addCriterion("visitChart =", value, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartNotEqualTo(Integer value) {
            addCriterion("visitChart <>", value, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartGreaterThan(Integer value) {
            addCriterion("visitChart >", value, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartGreaterThanOrEqualTo(Integer value) {
            addCriterion("visitChart >=", value, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartLessThan(Integer value) {
            addCriterion("visitChart <", value, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartLessThanOrEqualTo(Integer value) {
            addCriterion("visitChart <=", value, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartIn(List<Integer> values) {
            addCriterion("visitChart in", values, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartNotIn(List<Integer> values) {
            addCriterion("visitChart not in", values, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartBetween(Integer value1, Integer value2) {
            addCriterion("visitChart between", value1, value2, "visitchart");
            return (Criteria) this;
        }

        public Criteria andVisitchartNotBetween(Integer value1, Integer value2) {
            addCriterion("visitChart not between", value1, value2, "visitchart");
            return (Criteria) this;
        }

        public Criteria andGroupadminIsNull() {
            addCriterion("groupAdmin is null");
            return (Criteria) this;
        }

        public Criteria andGroupadminIsNotNull() {
            addCriterion("groupAdmin is not null");
            return (Criteria) this;
        }

        public Criteria andGroupadminEqualTo(Integer value) {
            addCriterion("groupAdmin =", value, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminNotEqualTo(Integer value) {
            addCriterion("groupAdmin <>", value, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminGreaterThan(Integer value) {
            addCriterion("groupAdmin >", value, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminGreaterThanOrEqualTo(Integer value) {
            addCriterion("groupAdmin >=", value, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminLessThan(Integer value) {
            addCriterion("groupAdmin <", value, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminLessThanOrEqualTo(Integer value) {
            addCriterion("groupAdmin <=", value, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminIn(List<Integer> values) {
            addCriterion("groupAdmin in", values, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminNotIn(List<Integer> values) {
            addCriterion("groupAdmin not in", values, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminBetween(Integer value1, Integer value2) {
            addCriterion("groupAdmin between", value1, value2, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andGroupadminNotBetween(Integer value1, Integer value2) {
            addCriterion("groupAdmin not between", value1, value2, "groupadmin");
            return (Criteria) this;
        }

        public Criteria andTagFlagIsNull() {
            addCriterion("tag_flag is null");
            return (Criteria) this;
        }

        public Criteria andTagFlagIsNotNull() {
            addCriterion("tag_flag is not null");
            return (Criteria) this;
        }

        public Criteria andTagFlagEqualTo(Integer value) {
            addCriterion("tag_flag =", value, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagNotEqualTo(Integer value) {
            addCriterion("tag_flag <>", value, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagGreaterThan(Integer value) {
            addCriterion("tag_flag >", value, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("tag_flag >=", value, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagLessThan(Integer value) {
            addCriterion("tag_flag <", value, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagLessThanOrEqualTo(Integer value) {
            addCriterion("tag_flag <=", value, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagIn(List<Integer> values) {
            addCriterion("tag_flag in", values, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagNotIn(List<Integer> values) {
            addCriterion("tag_flag not in", values, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagBetween(Integer value1, Integer value2) {
            addCriterion("tag_flag between", value1, value2, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andTagFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("tag_flag not between", value1, value2, "tagFlag");
            return (Criteria) this;
        }

        public Criteria andUsernameLikeInsensitive(String value) {
            addCriterion("upper(userName) like", value.toUpperCase(), "username");
            return (Criteria) this;
        }

        public Criteria andRealnameLikeInsensitive(String value) {
            addCriterion("upper(realName) like", value.toUpperCase(), "realname");
            return (Criteria) this;
        }

        public Criteria andEmailLikeInsensitive(String value) {
            addCriterion("upper(email) like", value.toUpperCase(), "email");
            return (Criteria) this;
        }

        public Criteria andMobileLikeInsensitive(String value) {
            addCriterion("upper(mobile) like", value.toUpperCase(), "mobile");
            return (Criteria) this;
        }

        public Criteria andTelephoneLikeInsensitive(String value) {
            addCriterion("upper(telephone) like", value.toUpperCase(), "telephone");
            return (Criteria) this;
        }

        public Criteria andAddressLikeInsensitive(String value) {
            addCriterion("upper(address) like", value.toUpperCase(), "address");
            return (Criteria) this;
        }

        public Criteria andPasswordLikeInsensitive(String value) {
            addCriterion("upper(password) like", value.toUpperCase(), "password");
            return (Criteria) this;
        }

        public Criteria andValidatecodeLikeInsensitive(String value) {
            addCriterion("upper(validateCode) like", value.toUpperCase(), "validatecode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}